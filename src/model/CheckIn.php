<?php
/**
 * Created by PhpStorm.
 * User: grent
 * Date: 1/21/2019
 * Time: 8:22 PM
 */

class CheckIn
{
    private $id;
    private $deskServiceId;
    private $ticketId;
    private $date;

    /**
     * checkIn constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDeskServiceId()
    {
        return $this->deskServiceId;
    }

    /**
     * @param mixed $deskServiceId
     */
    public function setDeskServiceId($deskServiceId)
    {
        $this->deskServiceId = $deskServiceId;
    }

    /**
     * @return mixed
     */
    public function getTicketId()
    {
        return $this->ticketId;
    }

    /**
     * @param mixed $ticketId
     */
    public function setTicketId($ticketId)
    {
        $this->ticketId = $ticketId;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }




}