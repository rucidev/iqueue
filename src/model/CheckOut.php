<?php
/**
 * Created by PhpStorm.
 * User: grent
 * Date: 1/21/2019
 * Time: 8:26 PM
 */

class CheckOut
{
  private $id;
  private $checkInId;
  private $ticketId;
  private $note;
  private $date;

    /**
     * checkOut constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCheckInId()
    {
        return $this->checkInId;
    }

    /**
     * @param mixed $checkInId
     */
    public function setCheckInId($checkInId)
    {
        $this->checkInId = $checkInId;
    }

    /**
     * @return mixed
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param mixed $note
     */
    public function setNote($note)
    {
        $this->note = $note;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getTicketId()
    {
        return $this->ticketId;
    }

    /**
     * @param mixed $ticketId
     */
    public function setTicketId($ticketId): void
    {
        $this->ticketId = $ticketId;
    }



}