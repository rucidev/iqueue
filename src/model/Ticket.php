<?php
/**
 * Created by PhpStorm.
 * User: grent
 * Date: 1/21/2019
 * Time: 8:33 PM
 */

class Ticket
{
  private $id;
  private $customer;
  private $deskService;
  private $business;
  private $status;
  private $count;
  private $isCheckedIn;
  private $date;

    /**
     * ticket constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param mixed $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return mixed
     */
    public function getDeskService()
    {
        return $this->deskService;
    }

    /**
     * @param mixed $deskService
     */
    public function setDeskService($deskService)
    {
        $this->deskService = $deskService;
    }


    /**
     * @return mixed
     */
    public function getBusiness()
    {
        return $this->business;
    }

    /**
     * @param mixed $business
     */
    public function setBusiness($business): void
    {
        $this->business = $business;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param mixed $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @return mixed
     */
    public function getIsCheckedIn()
    {
        return $this->isCheckedIn;
    }

    /**
     * @param mixed $isCheckedIn
     */
    public function setIsCheckedIn($isCheckedIn): void
    {
        $this->isCheckedIn = $isCheckedIn;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }




}