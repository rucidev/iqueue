<?php
/**
 * Created by PhpStorm.
 * User: Aleks Ruci
 * Date: 22.01.2019
 * Time: 16:53
 */

require_once 'DBLayer.php';
require_once '../model/Rate.php';
require_once '../model/TicketStatus.php';
require_once '../model/User.php';
require_once '../model/UserType.php';

class UserDAO extends DBLayer
{
    public function __construct()
    {
        parent::__construct();
    }

    // Start User

    public function getUsers($filter)
    {
        $query = "SELECT user.*, " .
                 "       usertype.Name AS TypeName " .
                 "FROM user " .
                 "INNER JOIN usertype ON user.TypeID = usertype.ID " .
                 "WHERE 1 = 1 ";

        if ($filter['id'] != null) {
            $query .=" AND user.ID = " . $this->getRealEscapeString($filter['id']);
        }
        if ($filter['username'] != null) {
            $query .=" AND user.Username = '{$this->getRealEscapeString($filter['username'])}'";
        }
        if ($filter['password'] != null) {
            $query .=" AND user.Password = '{$this->getRealEscapeString($filter['password'])}'";
        }
        if ($filter['nameSurname'] != null) {
            $query .=" AND (user.Name LIKE '%" . $this->getRealEscapeString($filter['nameSurname']) . "%' OR user.Surname LIKE '%" . $this->getRealEscapeString($filter['nameSurname']) . "%' ";
        }
        if ($filter['phone'] != null) {
            $query .=" AND user.Phone LIKE '%" . $this->getRealEscapeString($filter['phone']) . ")%' ";
        }
        if ($filter['typeId'] != null) {
            $query .=" AND user.TypeID = " . $this->getRealEscapeString($filter['typeId']);
        }


        $result = $this->executeQuery($query);
        $users = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $user = new User($row['ID']);
            $user->setUsername($row['Username']);
            $user->setPassword($row['Password']);
            $user->setName($row['Name']);
            $user->setSurname($row['Surname']);
            $user->setEmail($row['Email']);
            $user->setPhone($row['Phone']);

            $userType = new UserType($row['TypeID']);
            $userType->setName($row['TypeName']);
            $user->setType($userType);

            $user->setIsActive($row['IsActive']);

            array_push($users, $user);
        }
        return $users;
    }

    public function saveUser(User $user)
    {
        $query = "INSERT INTO user(Username, Password, Name, Surname, Email, Phone, TypeID) " .
            "VALUES ('{$this->getRealEscapeString($user->getUsername())}' " .
            "      , '".md5($this->getRealEscapeString($user->getPassword()))."' " .
            "      , '{$this->getRealEscapeString($user->getName())}' " .
            "      , '{$this->getRealEscapeString($user->getSurname())}' " .
            "      , '{$this->getRealEscapeString($user->getEmail())}' " .
            "      , '{$this->getRealEscapeString($user->getPhone())}' " .
            "      , {$this->getRealEscapeString($user->getType())})";
        $this->executeQuery($query);
        return $this->getGeneratedId();
    }

    public function saveUserWithActive(User $user)
    {
        $query = "INSERT INTO user(Username, Password, Name, Surname, Email, Phone, TypeID, IsActive) " .
            "VALUES ('{$this->getRealEscapeString($user->getUsername())}' " .
            "      , '".md5($this->getRealEscapeString($user->getPassword()))."' " .
            "      , '{$this->getRealEscapeString($user->getName())}' " .
            "      , '{$this->getRealEscapeString($user->getSurname())}' " .
            "      , '{$this->getRealEscapeString($user->getEmail())}' " .
            "      , '{$this->getRealEscapeString($user->getPhone())}' " .
            "      , {$this->getRealEscapeString($user->getType())} " .
            "      , {$this->getRealEscapeString($user->getisActive())})";
        $this->executeQuery($query);
        return $this->getGeneratedId();
    }

    public function updateUser(User $user)
    {
        $query = "UPDATE `user` SET 
             `Username` = '{$this->getRealEscapeString($user->getUsername())}',
             `Password` = '".md5($this->getRealEscapeString($user->getPassword()))."',
             `Name` = '{$this->getRealEscapeString($user->getName())}', 
             `Surname` = '{$this->getRealEscapeString($user->getSurname())}', 
             `Email` = '{$this->getRealEscapeString($user->getEmail())}',
             `Phone` = '{$this->getRealEscapeString($user->getPhone())}',
             `IsActive` = '{$this->getRealEscapeString($user->getisActive())}'
              WHERE `ID` = {$this->getRealEscapeString($user->getId())}";


        $this->executeQuery($query);
    }

    public function updateUserNoPassword(User $user)
    {
        $query = "UPDATE `user` SET 
             `Username` = '{$this->getRealEscapeString($user->getUsername())}',
             `Name` = '{$this->getRealEscapeString($user->getName())}', 
             `Surname` = '{$this->getRealEscapeString($user->getSurname())}', 
             `Email` = '{$this->getRealEscapeString($user->getEmail())}',
             `Phone` = '{$this->getRealEscapeString($user->getPhone())}',
             `IsActive` = '{$this->getRealEscapeString($user->getisActive())}'
              WHERE `ID` = {$this->getRealEscapeString($user->getId())}";


        $this->executeQuery($query);
        return mysqli_affected_rows($this->getDB());
    }

    public function updateUserStatus($userId, $status)
    {
        $query = "UPDATE user " .
                 "SET IsActive = {$this->getRealEscapeString($status)} " .
                 "WHERE ID = {$this->getRealEscapeString($userId)}";

        $this->executeQuery($query);
    }

    public function deleteUser($id)
    {
        $query = "DELETE FROM user WHERE ID = {$this->getRealEscapeString($id)} ";
        $this->executeQuery($query);
    }

    // End User

    // Start UserType

    public function getUserTypes()
    {
        $query = "SELECT * FROM usertype";

        $result = $this->executeQuery($query);
        $userTypes = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $userType = new UserType($row['ID']);
            $userType->setName($row['Name']);

            array_push($userTypes, $userType);
        }
        return $userTypes;
    }

    // End UserType

    //Start Rate

    public function getRates($filter)
    {
        $query = "SELECT  rate.*, " .
                 "        user.Name AS CustomerName, " .
                 "        user.Surname AS CustomerSurname, " .
                 "        user.Phone AS CustomerPhone, " .
                 "FROM rate " .
                 "INNER JOIN ticket ON rate.TicketID = ticket.ID " .
                 "INNER JOIN user ON ticket.CustomerID = user.ID " .
                 "INNER JOIN deskservice ON ticket.DeskServiceID = deskservice.ID " .
                 "WHERE 1 = 1 ";

        if ($filter['id'] != null) {
            $query .= " AND rate.ID = " . $this->getRealEscapeString($filter['id']);
        }
        if ($filter['ticketId'] != null) {
            $query .= " AND rate.TicketID = " . $this->getRealEscapeString($filter['ticketId']);
        }
        if ($filter['customerId'] != null) {
            $query .= " AND user.ID = " . $this->getRealEscapeString($filter['customerId']);
        }
        if ($filter['deskServiceId'] != null) {
            $query .= " AND deskservice.ID = " . $this->getRealEscapeString($filter['deskServiceId']);
        }

        $result = $this->executeQuery($query);
        $rates = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $rate = new Rate($row['ID']);

            $customer = new User($row['CustomerID']);
            $customer->setName($row['CustomerName']);
            $customer->setSurname($row['CustomerSurname']);
            $customer->setPhone($row['CustomerPhone']);
            $rate->setCustomer($customer);

            $rate->setStars($row['Starts']);
            $rate->setNote($row['Note']);

            array_push($rates, $rate);
        }
        return $rates;
    }

    public function saveRate(Rate $rate)
    {
        $query = "INSERT INTO rate(TicketID, Stars, Note) " .
            "VALUES ({$this->getRealEscapeString($rate->getTicketId())} " .
            "      , {$this->getRealEscapeString($rate->getStars())} " .
            "      , {$rate->getNote()}) ";

        $this->executeQuery($query);
    }

    public function deleteRate($id)
    {
        $query = "DELETE FROM rate WHERE ID = {$this->getRealEscapeString($id)} ";
        $this->executeQuery($query);
    }

    //TODO move this to BusinessDAO
    public function getDeskServiceAvgRate($deskServiceId) {
        $query = "SELECT AVG(Starts) AS AvgRate" .
                 "FROM rate " .
                 "INNER JOIN ticket ON rate.TicketID = ticket.ID " .
                 "INNER JOIN deskservice ON ticket.DeskServiceID = deskservice.ID " .
                 "WHERE ticket.StatusID = " . TicketStatus::$COMPLETED . " " .
                 "AND ticket.deskservice.ID = {$this->getRealEscapeString($deskServiceId)}";

        $result = $this->executeQuery($query);
        $row = mysqli_fetch_assoc($result);
        return $row['AvgRate'];
    }

    // End Rates
}