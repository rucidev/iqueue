-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 21, 2019 at 09:41 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `web18_gmustafa16`
--

-- --------------------------------------------------------

--
-- Table structure for table `business`
--

CREATE TABLE `business` (
  `Id` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Address` varchar(50) NOT NULL,
  `Phone` varchar(50) NOT NULL,
  `WebsiteURL` varchar(50) NOT NULL,
  `ImgURL` varchar(50) NOT NULL,
  `IsActive` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `businessheader`
--

CREATE TABLE `businessheader` (
  `Id` int(11) NOT NULL,
  `BusinessId` int(11) NOT NULL,
  `EmployeeId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `businessoperatinghour`
--

CREATE TABLE `businessoperatinghour` (
  `Id` int(11) NOT NULL,
  `BusinessId` int(11) NOT NULL,
  `StartTime` time NOT NULL,
  `EndTime` time NOT NULL,
  `DayOfTheWeek` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `checkin`
--

CREATE TABLE `checkin` (
  `Id` int(11) NOT NULL,
  `DeskServiceId` int(11) NOT NULL,
  `TicketId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `checkout`
--

CREATE TABLE `checkout` (
  `Id` int(11) NOT NULL,
  `CheckInId` int(11) NOT NULL,
  `Note` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `deskservice`
--

CREATE TABLE `deskservice` (
  `Id` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `BusinessId` int(11) NOT NULL,
  `Etc` int(11) NOT NULL,
  `ImgURL` varchar(50) NOT NULL,
  `SportelistId` int(11) NOT NULL,
  `Counter` int(11) NOT NULL DEFAULT '1',
  `IsActive` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rate`
--

CREATE TABLE `rate` (
  `Id` int(11) NOT NULL,
  `TicketId` int(11) NOT NULL,
  `Stars` int(11) NOT NULL,
  `Note` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

CREATE TABLE `ticket` (
  `Id` int(11) NOT NULL,
  `CustomerId` int(11) NOT NULL,
  `DeskServiceId` int(11) NOT NULL,
  `TicketStatusId` int(11) NOT NULL,
  `Count` int(11) NOT NULL,
  `Date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ticketstatus`
--

CREATE TABLE `ticketstatus` (
  `Id` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `Id` int(11) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Surname` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Phone` varchar(50) NOT NULL,
  `TypeId` int(11) NOT NULL,
  `IsActive` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `usertype`
--

CREATE TABLE `usertype` (
  `Id` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `business`
--
ALTER TABLE `business`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `businessheader`
--
ALTER TABLE `businessheader`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `BusinessId` (`BusinessId`),
  ADD KEY `UserId` (`EmployeeId`);

--
-- Indexes for table `businessoperatinghour`
--
ALTER TABLE `businessoperatinghour`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `BusinessId` (`BusinessId`);

--
-- Indexes for table `checkin`
--
ALTER TABLE `checkin`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `DeskServiceId` (`DeskServiceId`),
  ADD KEY `QueueId` (`TicketId`);

--
-- Indexes for table `checkout`
--
ALTER TABLE `checkout`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `CheckInId` (`CheckInId`);

--
-- Indexes for table `deskservice`
--
ALTER TABLE `deskservice`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `BusinessId` (`BusinessId`),
  ADD KEY `UserId` (`SportelistId`);

--
-- Indexes for table `rate`
--
ALTER TABLE `rate`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `TicketId` (`TicketId`);

--
-- Indexes for table `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `QueueStatusId` (`TicketStatusId`),
  ADD KEY `DesktopServiceId` (`DeskServiceId`),
  ADD KEY `UserId` (`CustomerId`);

--
-- Indexes for table `ticketstatus`
--
ALTER TABLE `ticketstatus`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `TypeId` (`TypeId`);

--
-- Indexes for table `usertype`
--
ALTER TABLE `usertype`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `business`
--
ALTER TABLE `business`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `businessheader`
--
ALTER TABLE `businessheader`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `businessoperatinghour`
--
ALTER TABLE `businessoperatinghour`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `checkin`
--
ALTER TABLE `checkin`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `checkout`
--
ALTER TABLE `checkout`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deskservice`
--
ALTER TABLE `deskservice`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rate`
--
ALTER TABLE `rate`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket`
--
ALTER TABLE `ticket`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticketstatus`
--
ALTER TABLE `ticketstatus`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `usertype`
--
ALTER TABLE `usertype`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `businessheader`
--
ALTER TABLE `businessheader`
  ADD CONSTRAINT `businessheader_ibfk_1` FOREIGN KEY (`BusinessId`) REFERENCES `business` (`Id`),
  ADD CONSTRAINT `businessheader_ibfk_2` FOREIGN KEY (`EmployeeId`) REFERENCES `user` (`Id`);

--
-- Constraints for table `businessoperatinghour`
--
ALTER TABLE `businessoperatinghour`
  ADD CONSTRAINT `businessoperatinghour_ibfk_1` FOREIGN KEY (`BusinessId`) REFERENCES `business` (`Id`);

--
-- Constraints for table `checkin`
--
ALTER TABLE `checkin`
  ADD CONSTRAINT `checkin_ibfk_1` FOREIGN KEY (`DeskServiceId`) REFERENCES `deskservice` (`Id`),
  ADD CONSTRAINT `checkin_ibfk_2` FOREIGN KEY (`TicketId`) REFERENCES `ticket` (`Id`);

--
-- Constraints for table `checkout`
--
ALTER TABLE `checkout`
  ADD CONSTRAINT `checkout_ibfk_1` FOREIGN KEY (`CheckInId`) REFERENCES `checkin` (`Id`);

--
-- Constraints for table `deskservice`
--
ALTER TABLE `deskservice`
  ADD CONSTRAINT `deskservice_ibfk_1` FOREIGN KEY (`BusinessId`) REFERENCES `business` (`Id`),
  ADD CONSTRAINT `deskservice_ibfk_2` FOREIGN KEY (`SportelistId`) REFERENCES `user` (`Id`);

--
-- Constraints for table `rate`
--
ALTER TABLE `rate`
  ADD CONSTRAINT `rate_ibfk_1` FOREIGN KEY (`TicketId`) REFERENCES `ticket` (`Id`);

--
-- Constraints for table `ticket`
--
ALTER TABLE `ticket`
  ADD CONSTRAINT `ticket_ibfk_1` FOREIGN KEY (StatusID) REFERENCES `ticketstatus` (`Id`),
  ADD CONSTRAINT `ticket_ibfk_2` FOREIGN KEY (`DeskServiceId`) REFERENCES `deskservice` (`Id`),
  ADD CONSTRAINT `ticket_ibfk_3` FOREIGN KEY (`CustomerId`) REFERENCES `user` (`Id`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`TypeId`) REFERENCES `usertype` (`Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


