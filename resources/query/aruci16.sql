-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2019 at 02:44 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `web18_aruci16`
--

-- --------------------------------------------------------

--
-- Table structure for table `business`
--

CREATE TABLE `business` (
  `ID` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Address` varchar(50) NOT NULL,
  `Phone` varchar(50) NOT NULL,
  `WebsiteURL` varchar(50) NOT NULL,
  `ImgURL` varchar(50) NOT NULL,
  `IsActive` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `businessemployeeheader`
--

CREATE TABLE `businessemployeeheader` (
  `ID` int(11) NOT NULL,
  `BusinessID` int(11) NOT NULL,
  `EmployeeID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `businessoperatinghour`
--

CREATE TABLE `businessoperatinghour` (
  `ID` int(11) NOT NULL,
  `BusinessID` int(11) NOT NULL,
  `StartTime` time NOT NULL,
  `EndTime` time NOT NULL,
  `DayOfTheWeek` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `checkin`
--

CREATE TABLE `checkin` (
  `ID` int(11) NOT NULL,
  `DeskServiceID` int(11) NOT NULL,
  `TicketID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `checkout`
--

CREATE TABLE `checkout` (
  `ID` int(11) NOT NULL,
  `CheckInID` int(11) NOT NULL,
  `Note` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `deskservice`
--

CREATE TABLE `deskservice` (
  `ID` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `BusinessID` int(11) NOT NULL,
  `ETC` int(11) NOT NULL,
  `ImgURL` varchar(50) NOT NULL,
  `SportelistID` int(11) NOT NULL,
  `Counter` int(11) NOT NULL DEFAULT '1',
  `IsActive` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rate`
--

CREATE TABLE `rate` (
  `ID` int(11) NOT NULL,
  `TicketID` int(11) NOT NULL,
  `Stars` int(11) NOT NULL,
  `Note` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

CREATE TABLE `ticket` (
  `ID` int(11) NOT NULL,
  `CustomerID` int(11) NOT NULL,
  `DeskServiceID` int(11) NOT NULL,
  `StatusID` int(11) NOT NULL,
  `Count` int(11) NOT NULL,
  `Date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ticketstatus`
--

CREATE TABLE `ticketstatus` (
  `ID` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `ID` int(11) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Surname` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Phone` varchar(50) NOT NULL,
  `TypeID` int(11) NOT NULL,
  `IsActive` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `usertype`
--

CREATE TABLE `usertype` (
  `ID` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `business`
--
ALTER TABLE `business`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `business_ID_uindex` (`ID`);

--
-- Indexes for table `businessemployeeheader`
--
ALTER TABLE `businessemployeeheader`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `businessemployeeheader_ID_uindex` (`ID`),
  ADD KEY `BusinessID` (`BusinessID`),
  ADD KEY `UserID` (`EmployeeID`);

--
-- Indexes for table `businessoperatinghour`
--
ALTER TABLE `businessoperatinghour`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `businessoperatinghour_ID_uindex` (`ID`),
  ADD KEY `BusinessID` (`BusinessID`);

--
-- Indexes for table `checkin`
--
ALTER TABLE `checkin`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `checkin_ID_uindex` (`ID`),
  ADD KEY `DeskServiceID` (`DeskServiceID`),
  ADD KEY `QueueID` (`TicketID`);

--
-- Indexes for table `checkout`
--
ALTER TABLE `checkout`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `checkout_ID_uindex` (`ID`),
  ADD KEY `CheckInID` (`CheckInID`);

--
-- Indexes for table `deskservice`
--
ALTER TABLE `deskservice`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `deskservice_ID_uindex` (`ID`),
  ADD KEY `BusinessID` (`BusinessID`),
  ADD KEY `UserID` (`SportelistID`);

--
-- Indexes for table `rate`
--
ALTER TABLE `rate`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `rate_ID_uindex` (`ID`),
  ADD KEY `TicketID` (`TicketID`);

--
-- Indexes for table `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ticket_ID_uindex` (`ID`),
  ADD KEY `QueueStatusID` (`StatusID`),
  ADD KEY `DesktopServiceID` (`DeskServiceID`),
  ADD KEY `UserID` (`CustomerID`);

--
-- Indexes for table `ticketstatus`
--
ALTER TABLE `ticketstatus`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ticketstatus_ID_uindex` (`ID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `user_ID_uindex` (`ID`),
  ADD KEY `TypeID` (`TypeID`);

--
-- Indexes for table `usertype`
--
ALTER TABLE `usertype`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `business`
--
ALTER TABLE `business`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `businessemployeeheader`
--
ALTER TABLE `businessemployeeheader`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `businessoperatinghour`
--
ALTER TABLE `businessoperatinghour`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `checkin`
--
ALTER TABLE `checkin`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `checkout`
--
ALTER TABLE `checkout`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deskservice`
--
ALTER TABLE `deskservice`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rate`
--
ALTER TABLE `rate`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket`
--
ALTER TABLE `ticket`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticketstatus`
--
ALTER TABLE `ticketstatus`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `usertype`
--
ALTER TABLE `usertype`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `businessemployeeheader`
--
ALTER TABLE `businessemployeeheader`
  ADD CONSTRAINT `businessemployeeheader_ibfk_1` FOREIGN KEY (`BusinessID`) REFERENCES `business` (`ID`),
  ADD CONSTRAINT `businessemployeeheader_ibfk_2` FOREIGN KEY (`EmployeeID`) REFERENCES `user` (`ID`);

--
-- Constraints for table `businessoperatinghour`
--
ALTER TABLE `businessoperatinghour`
  ADD CONSTRAINT `businessoperatinghour_business_ID_fk` FOREIGN KEY (`BusinessID`) REFERENCES `business` (`ID`);

--
-- Constraints for table `checkin`
--
ALTER TABLE `checkin`
  ADD CONSTRAINT `checkin_ibfk_1` FOREIGN KEY (`DeskServiceID`) REFERENCES `deskservice` (`ID`),
  ADD CONSTRAINT `checkin_ibfk_2` FOREIGN KEY (`TicketID`) REFERENCES `ticket` (`ID`);

--
-- Constraints for table `checkout`
--
ALTER TABLE `checkout`
  ADD CONSTRAINT `checkout_ibfk_1` FOREIGN KEY (`CheckInID`) REFERENCES `checkin` (`ID`);

--
-- Constraints for table `deskservice`
--
ALTER TABLE `deskservice`
  ADD CONSTRAINT `deskservice_ibfk_1` FOREIGN KEY (`BusinessID`) REFERENCES `business` (`ID`),
  ADD CONSTRAINT `deskservice_ibfk_2` FOREIGN KEY (`SportelistID`) REFERENCES `user` (`ID`);

--
-- Constraints for table `rate`
--
ALTER TABLE `rate`
  ADD CONSTRAINT `rate_ibfk_1` FOREIGN KEY (`TicketID`) REFERENCES `ticket` (`ID`);

--
-- Constraints for table `ticket`
--
ALTER TABLE `ticket`
  ADD CONSTRAINT `ticket_ibfk_1` FOREIGN KEY (`StatusID`) REFERENCES `ticketstatus` (`ID`),
  ADD CONSTRAINT `ticket_ibfk_2` FOREIGN KEY (`DeskServiceID`) REFERENCES `deskservice` (`ID`),
  ADD CONSTRAINT `ticket_ibfk_3` FOREIGN KEY (`CustomerID`) REFERENCES `user` (`ID`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`TypeID`) REFERENCES `usertype` (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
