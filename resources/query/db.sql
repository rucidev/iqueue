/**
 * Created by PhpStorm.
 * User: Aleks Ruci
 * Date: 22.01.2019
 * Time: 17:42
 */

-- Add Date to check in and check out
ALTER TABLE checkin ADD Date datetime NOT NULL;
ALTER TABLE checkout ADD Date datetime NOT NULL;

-- Insert into web18_aru16_usertype
INSERT INTO web18_aruci16.usertype (ID, Name) VALUES (1, 'iQueue Admin');
INSERT INTO web18_aruci16.usertype (ID, Name) VALUES (2, 'Business Manager');
INSERT INTO web18_aruci16.usertype (ID, Name) VALUES (3, 'Sportelist');
INSERT INTO web18_aruci16.usertype (ID, Name) VALUES (4, 'Customer');

-- Insert into web18_aruci16_ticketstatus
INSERT INTO web18_aruci16.ticketstatus (ID, Name) VALUES (1, 'IN QUEUE');
INSERT INTO web18_aruci16.ticketstatus (ID, Name) VALUES (2, 'COMPLETED');
INSERT INTO web18_aruci16.ticketstatus (ID, Name) VALUES (3, 'CANCELED');

-- Set ImgURL size to 250 chars in business
ALTER TABLE business MODIFY ImgURL varchar(250) NOT NULL;

-- Insert to business table
ALTER TABLE business AUTO_INCREMENT = 1;
INSERT INTO `business` (`ID`, `Name`, `Address`, `Phone`, `WebsiteURL`, `ImgURL`, `IsActive`) VALUES (NULL, 'Posta Shqiptare', 'Rruga Reshit Collaku nr .4 , Tirana, ALBANIA', '08004141', 'https://www.postashqiptare.al/', 'https://www.postashqiptare.al/wp-content/uploads/2018/01/logoposta.png', '1');
INSERT INTO `business` (`ID`, `Name`, `Address`, `Phone`, `WebsiteURL`, `ImgURL`, `IsActive`) VALUES (NULL, 'AlbTelecom', 'Autostrada Tirane - Durres, Km. 7, ', '042200149', 'https://www.albtelecom.al/al/individuale/', 'https://www.albtelecom.al/media/2180/logo_slogan_al_white.svg', '1');

-- Set ImgURL not null and set ImgURL size to 250 chars
ALTER TABLE business MODIFY WebsiteURL varchar(250) NOT NULL;
ALTER TABLE business MODIFY ImgURL varchar(250);

INSERT INTO `user` (`ID`, `Username`, `Password`, `Name`, `Surname`, `Email`, `Phone`, `TypeID`, `IsActive`) VALUES (NULL, 'grent', MD5('root'), 'Grent', 'Mustafa', 'gmustafa16@gmail.com', '123456', '4', '1');

-- Set ImgURL in deskservice not null
ALTER TABLE deskservice MODIFY ImgURL varchar(50);

INSERT INTO web18_aruci16.user (ID, Username, Password, Name, Surname, Email, Phone, TypeID, IsActive) VALUES (2, 'aruci16', '63a9f0ea7bb98050796b649e85481845', 'Aleksandros', 'Ruci', 'aruci16@epoka.edu.al', '0699312253', 3, 1);

INSERT INTO web18_aruci16.deskservice (ID, Name, BusinessID, ETC, ImgURL, SportelistID, Counter, IsActive) VALUES (1, 'Koliposte', 1, 10, '', 2, 1, 1);

INSERT INTO ticket (ID, CustomerID, DeskServiceID, StatusID, Count, Date) VALUES (1, 1, 1, 1, 1, '2019-01-26');
UPDATE deskservice SET Counter = 2 WHERE ID = 1;

ALTER TABLE deskservice MODIFY ImgURL varchar(250);

ALTER TABLE ticket MODIFY Date datetime NOT NULL;

CREATE UNIQUE INDEX CheckInIDUnique ON checkout (CheckInID);

INSERT INTO ticketstatus(Name) VALUES ('CHECKED IN');

ALTER TABLE checkout MODIFY Note varchar(250);

INSERT INTO `businessemployeeheader` (`ID`, `BusinessID`, `EmployeeID`) VALUES (NULL, '1', '3');

CREATE UNIQUE INDEX businessemployeeUnique ON businessemployeeheader (BusinessID, EmployeeID);

INSERT INTO `web18_aruci16`.`user` (`Username`, `Password`, `Name`, `Surname`, `Email`, `Phone`, `TypeID`, `IsActive`) VALUES ('genti_posta', '63a9f0ea7bb98050796b649e85481845', 'Genti', 'Ajeti', 'genti@posta.gmail.com', '3803834', 2, DEFAULT);

ALTER TABLE ticket ADD IsCheckedIn boolean DEFAULT 0 NOT NULL;
ALTER TABLE ticket
  MODIFY COLUMN IsCheckedIn boolean DEFAULT 0 NOT NULL AFTER Count;

CREATE UNIQUE INDEX UsernameUnique ON User (Username);

CREATE UNIQUE INDEX SportelistUnique ON deskservice (ID, SportelistID);